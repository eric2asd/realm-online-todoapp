//
//  Item.swift
//  iOSToDoApp
//
//  Created by 陳信毅 on 2018/9/5.
//  Copyright © 2018年 陳信毅. All rights reserved.
//

import Foundation
import RealmSwift

class Item: Object {
    
    @objc dynamic var itemId: String = UUID().uuidString
    @objc dynamic var body: String = ""
    @objc dynamic var isDone: Bool = false
    @objc dynamic var timestamp: Date = Date()
    
    override static func primaryKey() -> String? {
        return "itemId"
    }
    
}

